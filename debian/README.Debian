
bgpdump library not included
----------------------------
This package provides the bpgdump binary only. If you're in need of
the libbgpdump library, file a bugreport severity wishlist. Patches
welcome.


Running upstream's test
-----------------------
Upstream included some huge dump files to be tested for regressions
during build. They have been removed from the Debian source package as
they are pretty huge: Full upstream tarball: 51 Mbyte, after re-packing
some 43 kbyte.

To run the test during build:

* Download the full upstream tarball from
    https://github.com/RIPE-NCC/bgpdump/releases
* Unpack the test files:
$ tar --wildcards --strip-components=1 -xavf <upstream.tar>.gz '*/test_*'
* Add the list to include-binaries so dpkg-source will not complain:
$ ls -1 test_*/* >>debian/source/include-binaries

Then rebuild as usual.


quagga configuration
--------------------
Since quagga documentation (10.15 "Dump BGP packets and table") is a
bit terse, here a few hints:

Add to your bgpd configuration, depending on whether you're interested
in all messages or updates only:

    dump bgp all|updates <path> [<interval>]

Where <interval> is in seconds. The <path> is a filename that must
be writable by quagga. It may contain format specifiers as described
in strftime(3).
